#include "Status.h"

void CPlayerStats::EquipItem(CWeapon* weapon)
{
	EquippedWeapon = weapon;
}

void CPlayerStats::EquipItem(CArmour* armour)
{
	EquippedArmour = armour;
}

void CPlayerStats::AddMoney(int amount)
{
	iMoney += amount;
}

void CPlayerStats::RemoveMoney(int amount)
{
	iMoney -= amount;
}

void CPlayerStats::AddHp(int amount)
{
	iHp += amount;
	if (iHp > iMaxHp)
	{
		iHp = iMaxHp;
	}
}

void CPlayerStats::RemoveHp(int amount)
{
	iHp -= amount;
	if (iHp < 0)
	{
		iHp = 0;
	}
}

void CPlayerStats::PrintStats()
{
	cout << "Stats: " << endl;
	cout << "Name: " << strPlayerName << "Gold: " << iMoney << endl;
	cout << "HP: " << iHp << " Attack " << iAtk << endl;
	cout << "Equipped Items: ";
	EquippedWeapon->print();
	cout << endl;
	EquippedArmour->print();
}
