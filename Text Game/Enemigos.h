#pragma once
#include <iostream>
#include "Inventario.h"
#include "Items.h"

using namespace std;

class CEnemy
{
public:
	int iHp;
	int iAtk;
	int iDef;
	int iEnemyXp;
	string strName;
	//CWeapon* DSword = new CWeapon;
	CEnemy(int Nivel, int ID);

	~CEnemy();

};
