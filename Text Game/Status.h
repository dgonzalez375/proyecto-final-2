#pragma once
#include "Items.h"
#include "Player.h"

class CPlayerStats
{
public:
	string strPlayerName;
	int iMaxHp = 10;
	int iHp = 10;
	int iAtk = 1;
	int iMoney = 0;

	CWeapon* EquippedWeapon;
	CArmour* EquippedArmour;


	void EquipItem(CWeapon* weapon);

	void EquipItem(CArmour* armour);

	void AddMoney(int amount);

	void RemoveMoney(int amount);

	void AddHp(int amount);

	void RemoveHp(int amount);

	void PrintStats();
};
CPlayerStats stats;