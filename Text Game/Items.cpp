#include "Items.h"

void CItem::print()
{
	//Empty...
}

CWeapon::CWeapon(int swordtype)
{
	if (swordtype == 1)
	{
		ItemID = 1;
		iAtk = 10;
		strName = "Iron Sword";
		strDescription = "Sword from Iron";
		iValue = 20;
	}
	if (swordtype == 2)
	{
		ItemID = 2;
		iAtk = 5;
		strName = "Wooden Sword";
		strDescription = "Sword from burts wood";
		iValue = 6;
	}
	if (swordtype == 3)
	{
		ItemID = 3;
		iAtk = 20;
		strName = "diamond Sword";
		strDescription = "look at my sword sword my diamond sword, you cannot afford my diamond";
		iValue = 55;
	}
}

void CWeapon::operator=(CWeapon item)
{
	strName = item.strName;
	strDescription = item.strDescription;
	iValue = item.iValue;
}

void CWeapon::print()
{
	cout << strName << "(" << iAtk << "atk)" << "Type: " << strDescription << endl;
}

CPotion::CPotion(int potiontype)
{
	if (potiontype == 1)
	{
		ItemID = 4;
		strName = "Not great Health potion";
		strDescription = "Gives health";
		health = 5;
		iValue = 10;
	}
	if (potiontype == 2)
	{
		ItemID = 5;
		strName = " great Health potion";
		strDescription = "Gives health";
		health = 20;
		iValue = 20;
	}
	if (potiontype == 3)
	{
		ItemID = 6;
		strName = "Greater Health potion";
		strDescription = "Gives health";
		health = 30;
		iValue = 50;
	}
}

void CPotion::print()
{
	cout << strName << "(" << health << "hp)" << "Description: " << strDescription << endl;
}

CArmour::CArmour(int armourtype) 
{
	if (armourtype == 1)
	{
		ItemID = 7;
		strName = "Wood Armour";
		strDescription = "Armour made from Wood";
		def = 5;
		iValue = 5;
	}
	if (armourtype == 2)
	{
		ItemID = 8;
		strName = "Iron Armour";
		strDescription = "Iron based defense";
		def = 10;
		iValue = 20;
	}
	if (armourtype == 3)
	{
		ItemID = 9;
		strName = "Diamond Armour";
		strDescription = "Bling Bling";
		def = 20;
		iValue = 50;
	}
}

void CArmour::print()
{
	cout << strName << "(" << def << " def)" << "Description: " << strDescription << endl;
}

void CArmour::operator=(CArmour item)
{
	strName = item.strName;
	strDescription = item.strDescription;
	iValue = item.iValue;
}
