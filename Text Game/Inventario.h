#pragma once
#include <vector>
#include <iostream>
#include "Items.h"
#include "Status.h"

using namespace std;

class CInventoryManager
{
public:
	vector<CWeapon*> WeaponInventory;
	vector<CPotion*> PotionInventory;
	vector<CArmour*> ArmourInventory;
	
	void AddItem(CWeapon* item);

	void AddItem(CPotion* item);

	void AddItem(CArmour* item);

	void UseItem(int itemindex);

	void SellItem(int itemindex, int option);

	void ShowInventory();
};

CInventoryManager Inv;