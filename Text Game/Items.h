#pragma once
#include <iostream>

using namespace std;

class CItem
{
public:
	virtual void print();
	int iValue;
	int ItemID;
	string strName;
	string strDescription;
};

class CWeapon :public CItem
{
public:
	int iAtk;
	CWeapon(int swordtype);
	void operator= (CWeapon item);
	void print();
};

class CPotion :public CItem
{
public:
	int health; //healing capacity of potion?
	CPotion(int potiontype);
	void print();
};

class CArmour :public CItem
{
public:
	int def;
	CArmour(int armourtype);
	void operator= (CArmour item);
	void print();
};