#pragma once

#include <vector>;
#include <iostream>;
#include "items.h"
#include "Status.h"

using namespace std;

class CPlayer
{
public:
	int iPHP;
	int iPAtk;
	int iPDef;
	int iXp;
	int iPNivel;
	int iXptoNextlvl;
	
	CPlayer();

	void lvlUp(int xp, int playernivel);
	void Status(string Nombre, string WeaponEquipped, string ArmourEqipped);
};



