#include "Enemigos.h"
#include<iostream>
using namespace std;

CEnemy::CEnemy(int Nivel, int ID)
{
	if (ID == 1)
	{
		//Goblin
		strName = "Goblin";
		iHp = Nivel * 2;
		iAtk = Nivel + 10;
		iDef = Nivel + 5;
		iEnemyXp = Nivel+2;
		cout << "Health Points: " << iHp << endl;
		cout << "Attack Power: " << iAtk << endl;
		cout << "Defense: " << iDef << endl;
	}
	if (ID == 2)
	{
		//Orc
		strName = "Orc";
		iHp = Nivel * 4;
		iAtk = Nivel + 15;
		iDef = Nivel + 8;
		iEnemyXp = Nivel + 8;
		cout << "Health Points: " << iHp << endl;
		cout << "Attack Power: " << iAtk << endl;
		cout << "Defense: " << iDef << endl;
	}

	if (ID == 3)
	{
		//Bandit
		strName = "Bandido";
		iHp = Nivel * 6;
		iAtk = Nivel + 20;
		iDef = Nivel + 10;
		iEnemyXp = Nivel + 10;
		cout << "Health Points: " << iHp << endl;
		cout << "Attack Power: " << iAtk << endl;
		cout << "Defense: " << iDef << endl;
	}
	if (ID == 3)
	{
		//Diablo
		strName = "Diablo";
		iHp = Nivel * 8;
		iAtk = Nivel + 22;
		iDef = Nivel + 14;
		iEnemyXp = Nivel + 20;
		cout << "Health Points: " << iHp << endl;
		cout << "Attack Power: " << iAtk << endl;
		cout << "Defense: " << iDef << endl;
	}

	if (ID == 5)
	{
		strName = "Final Boss";
		iHp = Nivel * 30;
		iAtk = Nivel + 20;
		iDef = Nivel + 30;
		iEnemyXp = 10000 + 2;
		cout << "Health Points: " << iHp << endl;
		cout << "Attack Power: " << iAtk << endl;
		cout << "Defense: " << iDef << endl;
	}
}
CEnemy::~CEnemy()
{
	//Inv.AddItem(DSword);
}
