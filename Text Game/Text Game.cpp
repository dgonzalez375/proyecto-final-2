// Text Game.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include <iostream>
#include <fstream>
#include<istream>
#include <sstream>
#include <vector>
#include <conio.h>
#include "Enemigos.h"
#include "Player.h"
#include "Inventario.h"

using namespace std;
vector <CItem*> Inventory;
vector <CArmour*> EArmour;
vector <CWeapon*> EWeapon;
/*void Ifinventory(CItem* base)
{
	for (int i = 0; i < 1; i++)
	{
		CWeapon* WeaponPointer = dynamic_cast<CWeapon*>(base);
		if (WeaponPointer)
		{
			WeaponPointer->print();
		}
		CPotion* PotionPointer = dynamic_cast<CPotion*>(base);
		if (PotionPointer)
		{
			PotionPointer->print();
		}
	}
}*/

/*void ItemRandomizer()
{
	int two;
	srand((unsigned)time(0));
	int RandItem = rand() % 2 +1;
		int itemtype = rand() % 2 + 1;
		cout << itemtype << endl;
		
		if (RandItem == 1)
		{
			switch (itemtype)
			{
			case 1:
			{
				CWeapon* Sword = new CWeapon(itemtype);
				
				break;
			}
			case 2:
			{
				CWeapon* Sword = new CWeapon(itemtype);
				
				break;
			}
			case 3:
			{
				CWeapon* Sword = new CWeapon(itemtype);
	
				break;
			}
			default:
				break;
			}
		}
		else if (RandItem == 2)
		{
			switch (itemtype)
			{
			case 1:
			{
				CPotion* Potion = new CPotion(itemtype);
			}
			case 2:
			{
				CPotion* Potion = new CPotion(itemtype);
			}
			case 3:
			{
				CPotion* Potion = new CPotion(itemtype);
				
			}
			default:
				break;
			}
		}

}*/

void Combat(int iEnemyNivel, int iID, CPlayer player)
{
	int fakelevl=5;
	bool bifCombat = true;
	int icombatmenuoption=0;
	CEnemy Enemy(iEnemyNivel, iID);
	char cCombatMenu = '1';
	bool bPlayerturn = true;
	char cAkeys = 'a';
	int icombatmenu=0;
	bool ifdefend = false;
	
	player.iPAtk = EWeapon[1]->iAtk;
	
	vector <string> menuCombat = { "1. Attack", "2. Defend", "3. Menu 3", "4. Run" };
	vector<CEnemy> Venemies;
	Venemies.push_back(Enemy);

	//combat loop
	while (bifCombat != false)
	{
		//player options
		while (bPlayerturn == true)
		{
			system("cls");
			cout << "Ahh man its a " << Venemies[0].strName << "oh no how terrible!!! Its of level " << iEnemyNivel << ". What are you going to do" << endl;
			//cout << "Potential Item Gained: " << Reward[0]->strName << endl;

			for (int i = 0; i < 4; i++) // imprint arrow based on keys
			{
				if (icombatmenuoption == i)
				{
					cout << "->";

				}
				else
				{
					cout << "";
				}
				cout << menuCombat[i] << endl;

			}
			cAkeys = _getch();
			switch (cAkeys)
			{
			case 's':
				if (icombatmenuoption < 4)
				{
					icombatmenuoption++;

					icombatmenu = icombatmenuoption;
				}
				break;
			case 'w':
				if (icombatmenuoption > 0)
				{
					icombatmenuoption--;

					icombatmenu = icombatmenuoption;
				}
				break;

			case 13:
				switch (icombatmenu)
				{
				case 0:
					cout << "I Attack" << endl;
					
					if (ifdefend == true)
					{
						Venemies[0].iHp -= (player.iPAtk - Venemies[0].iDef);
						ifdefend = false;
					}
					else
					{
						Venemies[0].iHp -= player.iPAtk;
					}
					cout <<"Enemy Health "<< Venemies[0].iHp << endl;
					bPlayerturn = false;
					//cout << "I have "<< Venemies[0].iHp <<" Hp" << endl;
					getchar();
					break;
				case 1:
					cout << "Defend" << endl;
					ifdefend = true;
					getchar();
					break;
				case 2:
					cout << "Usar Items" << endl;
					Inv.ShowInventory();
					getchar();
					break;
				case 3:
					srand((unsigned)time(0));
					cout << "Run" << endl;

					float iRandomEscapeChance;
					iRandomEscapeChance = rand() % ((Enemy.iDef + Enemy.iHp) * 3 / player.iPNivel);
					if (iRandomEscapeChance <= player.iPNivel)
					{

						//escape success
						cout << "escape success" << endl;
						bifCombat = false;
					}
					else
					{
						cout << "escape failed" << endl;
					}
					getchar();
					break;
				default:
					cout << "invalid menu item" << endl;
					getchar();
					break;
				}
				break;
			default:
				break;
			}
		}

		//check if dead enemy
		if (Venemies[0].iHp <= 0)
		{
			bifCombat = false;
			player.iXp += Venemies[0].iEnemyXp;
			cout << "Conseguistes " << Venemies[0].iEnemyXp<< " Xp!" << endl;
			int itemtype = rand() % 3 + 1;
			int itemclass = rand() % 3 + 1;
			if (itemtype == 1)
			{
				CWeapon* Sword = new CWeapon(itemclass);
				cout << "Conseguistes " <<Sword->strName << endl;
				Inv.AddItem(Sword);
			}
			if (itemtype == 3)
			{
				CArmour* Armour = new CArmour(itemclass);
				cout << "Conseguistes " << Armour->strName << endl;
				Inv.AddItem(Armour);
			}
			if (itemtype == 2)
			{
				CPotion* potion = new CPotion(itemclass);
				cout << "Conseguistes " << potion->strName << endl;
				Inv.AddItem(potion);
			}
			
			getchar();
		}

		//enbemy options
		while (bPlayerturn == false && Venemies[0].iHp>0)
		{
			srand((unsigned)time(0));
			int EnemyDecision = rand() % 2 + 1;

			if (EnemyDecision == 1)
			{
				cout << "The enemy attacks" << endl;
				//enemy attack
				if (ifdefend == true)
				{
					player.iPHP -= (Venemies[0].iAtk-player.iPDef);
					cout << "Dealing " << Venemies[0].iAtk - player.iPDef << " Damage" << endl;
				}
				else
				{
					player.iPHP -= Venemies[0].iAtk;
				}

				cout << "Player Health: " << player.iPHP << endl;
			}
			else if (EnemyDecision == 2)
			{
				cout << "The enemy defends!" << endl;
				//enemy defend
				ifdefend = true;
			}
			bPlayerturn = true;
		}
	}

}
void CombatBoss(int iEnemynivel, int iID, CPlayer player)
{
	//int fakelevl = 5;
	bool bifCombat = true;
	int icombatmenuoption = 0;
	CEnemy Enemy(iEnemynivel, iID);
	char cCombatMenu = '1';
	bool bPlayerturn = true;
	char cAkeys = 'a';
	int icombatmenu = 0;
	bool ifdefend = false;
	
	player.iPAtk = stats.EquippedWeapon->iAtk;
	player.iPDef = stats.EquippedArmour->def;
	vector <string> menuCombat = { "1. Attack", "2. Defend", "3. Menu 3", "4. Run" };
	vector<CEnemy> Venemies;
	Venemies.push_back(Enemy);

	CWeapon* Sword = new CWeapon(2);


	//combat loop
	while (bifCombat != false)
	{
		//player options
		while (bPlayerturn == true)
		{



			system("cls");
			cout << "Ahh man its a " << Venemies[0].strName << "oh no how terrible!!! Its of level " << iEnemynivel << ". What are you going to do" << endl;
			cout << "Potential Item Gained: " << Sword->strName << endl;

			for (int i = 0; i < 4; i++) // imprint arrow based on keys
			{
				if (icombatmenuoption == i)
				{
					cout << "->";

				}
				else
				{
					cout << "";
				}
				cout << menuCombat[i] << endl;

			}
			cAkeys = _getch();
			switch (cAkeys)
			{
			case 's':
				if (icombatmenuoption < 4)
				{
					icombatmenuoption++;

					icombatmenu = icombatmenuoption;
				}
				break;
			case 'w':
				if (icombatmenuoption > 0)
				{
					icombatmenuoption--;

					icombatmenu = icombatmenuoption;
				}
				break;

			case 13:
				switch (icombatmenu)
				{
				case 0:
					cout << "I Attack" << endl;

					if (ifdefend == true)
					{
						Venemies[0].iHp -= (player.iPAtk - Venemies[0].iDef);
						ifdefend = false;
					}
					else
					{
						Venemies[0].iHp -= player.iPAtk;
					}
					cout << "Enemy Health " << Venemies[0].iHp << endl;
					bPlayerturn = false;
					//cout << "I have "<< Venemies[0].iHp <<" Hp" << endl;
					getchar();
					break;
				case 1:
					cout << "Defend" << endl;
					ifdefend = true;
					getchar();
					break;
				case 2:
					cout << "Usar Items" << endl;
					getchar();
					break;
				case 3:
					srand((unsigned)time(0));
					cout << "Run" << endl;

					float iRandomEscapeChance;
					iRandomEscapeChance = rand() % ((Enemy.iDef + Enemy.iHp) * 3 / player.iPNivel);
					if (iRandomEscapeChance <= player.iPNivel)
					{

						//escape success
						cout << "escape success" << endl;
						bifCombat = false;
					}
					else
					{
						cout << "escape failed" << endl;
					}
					getchar();
					break;
				default:
					cout << "invalid menu item" << endl;
					getchar();
					break;
				}
				break;
			default:
				break;
			}
		}

		//check if dead enemy
		if (Venemies[0].iHp <= 0)
		{
			bifCombat = false;
			player.iXp += Venemies[0].iEnemyXp;
			cout << "Conseguistes " << Venemies[0].iEnemyXp << endl;
			getchar();
		}

		//enbemy options
		while (bPlayerturn == false && Venemies[0].iHp > 0)
		{
			srand((unsigned)time(0));
			int EnemyDecision = rand() % 2 + 1;

			if (EnemyDecision == 1)
			{
				cout << "The enemy attacks" << endl;
				//enemy attack
				if (ifdefend == true)
				{
					player.iPHP -= (Venemies[0].iAtk - player.iPDef);
					cout << "Dealing " << Venemies[0].iAtk - player.iPDef << " Damage" << endl;
				}
				else
				{
					player.iPHP -= Venemies[0].iAtk;
				}

				cout << "Player Health: " << player.iPHP << endl;
			}
			else if (EnemyDecision == 2)
			{
				cout << "The enemy defends!" << endl;
				//enemy defend
				ifdefend = true;
			}
			bPlayerturn = true;
		}
	}

}
void Explore(CPlayer player)
{
	int iExploretype =0;
	srand((unsigned)time(0));
	iExploretype = rand() % 10+1;

	if (iExploretype >=1 && iExploretype <= 3)
	{
		int iRandomNivel;
		int iRandomEnemy;
		iRandomNivel = rand() % (9+player.iPNivel) + (player.iPNivel);
		iRandomEnemy = rand() % 4 + 1;
		//iRandomEnemy = 1;
		cout << "ahhh combat" << endl;
		
		Combat(iRandomNivel, iRandomEnemy,player);
		
	}
	if (iExploretype >= 4 && iExploretype <= 6)
	{
		cout << "oooo encontrastes un item" << endl;
	}
	if (iExploretype >= 7 && iExploretype <= 9)
	{
		cout << "encontrastes dinero de tal cantidad" << endl;
	}
	if (iExploretype ==10 || iExploretype == 0)
	{
		cout << "bueno encontrastes nada" << endl;
	}
}

void Menutienda()
{
	//int itemtype = 1;
	int itemchosen;
	srand((unsigned)time(0));
	vector<CItem*> MenuItems;
	//vector<string> MenuItems2;
	for (int i = 0; i < 4; i++)
	{
		
		int itemtype = rand() % 9+1;
		int itemclass = rand() % 3 + 1;
		//cout << itemtype << endl;
		switch (itemtype)
		{
			case 1:
			{
				CWeapon* Sword = new CWeapon(1);
				MenuItems.push_back(Sword);
				break;
			}
			case 2:
			{
				CWeapon* Sword = new CWeapon(2);
				MenuItems.push_back(Sword);
				break;
			}
			case 3:
			{
				CWeapon* Sword = new CWeapon(3);
				MenuItems.push_back(Sword);
				break;
			}
			case 4:
			{
				CArmour* armour = new CArmour(1);
				MenuItems.push_back(armour);
				break;
			}
			case 5:
			{
				CArmour* armour = new CArmour(2);
				MenuItems.push_back(armour);
				break;
			}
			case 6:
			{
				CArmour* armour = new CArmour(3);
				MenuItems.push_back(armour);
				break;
			}
			case 7:
			{
				CPotion* potion = new CPotion(1);
				MenuItems.push_back(potion);
				break;
			}
			case 8:
			{
				CPotion* potion = new CPotion(2);
				MenuItems.push_back(potion);
				break;
			}
			case 9:
			{
				CPotion* potion = new CPotion(3);
				MenuItems.push_back(potion);
				break;
			}
			default:
				break;
		}
	}
	
	
	//print items name
	for (int i = 0; i < MenuItems.size(); i++)
	{
		cout << i+1 <<". "<< MenuItems[i]->strName << endl;
	}
	
	// choose item
	int iChooseitem=0;
	int yesno =0;
	switch (iChooseitem)
	{
	case 1:
		cout << "Quieres tener este item?" << endl;
		cout << "1. Si" << " 2. No" << endl;
		switch (yesno)
		{
		case 1:
			if (MenuItems[1]->ItemID == 1)
			{
				CWeapon* Sword = new CWeapon(1);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 2)
			{
				CWeapon* Sword = new CWeapon(2);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 3)
			{
				CWeapon* Sword = new CWeapon(1);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 4)
			{
				CPotion* potion = new CPotion(1);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 5)
			{
				CPotion* potion = new CPotion(2);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 6)
			{
				CPotion* potion = new CPotion(3);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 7)
			{
				CArmour* armour = new CArmour(1);
				Inv.AddItem(armour);
			}
			if (MenuItems[1]->ItemID == 8)
			{
				CArmour* armour = new CArmour(2);
				Inv.AddItem(armour);
			}
			if (MenuItems[1]->ItemID == 9)
			{
				CArmour* armour = new CArmour(3);
				Inv.AddItem(armour);
			}
			cout << "Item Added" << endl;
			getchar();
		case 2:
			break;
		default:
			break;
		}
	case 2:
		cout << "Quieres tener este item?" << endl;
		cout << "1. Si" << " 2. No" << endl;
		switch (yesno)
		{
		case 1:
			if (MenuItems[1]->ItemID == 1)
			{
				CWeapon* Sword = new CWeapon(1);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 2)
			{
				CWeapon* Sword = new CWeapon(2);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 3)
			{
				CWeapon* Sword = new CWeapon(1);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 4)
			{
				CPotion* potion = new CPotion(1);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 5)
			{
				CPotion* potion = new CPotion(2);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 6)
			{
				CPotion* potion = new CPotion(3);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 7)
			{
				CArmour* armour = new CArmour(1);
				Inv.AddItem(armour);
			}
			if (MenuItems[1]->ItemID == 8)
			{
				CArmour* armour = new CArmour(2);
				Inv.AddItem(armour);
			}
			if (MenuItems[1]->ItemID == 9)
			{
				CArmour* armour = new CArmour(3);
				Inv.AddItem(armour);
			}
			cout << "Item Added" << endl;
			getchar();
		case 2:
			break;
		default:
			break;
		}
	case 3:
		cout << "Quieres tener este item?" << endl;
		cout << "1. Si" << " 2. No" << endl;
		switch (yesno)
		{
		case 1:
			if (MenuItems[1]->ItemID == 1)
			{
				CWeapon* Sword = new CWeapon(1);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 2)
			{
				CWeapon* Sword = new CWeapon(2);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 3)
			{
				CWeapon* Sword = new CWeapon(1);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 4)
			{
				CPotion* potion = new CPotion(1);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 5)
			{
				CPotion* potion = new CPotion(2);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 6)
			{
				CPotion* potion = new CPotion(3);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 7)
			{
				CArmour* armour = new CArmour(1);
				Inv.AddItem(armour);
			}
			if (MenuItems[1]->ItemID == 8)
			{
				CArmour* armour = new CArmour(2);
				Inv.AddItem(armour);
			}
			if (MenuItems[1]->ItemID == 9)
			{
				CArmour* armour = new CArmour(3);
				Inv.AddItem(armour);
			}
			cout << "Item Added" << endl;
			getchar();
		case 2:
			break;
		default:
			break;
		}
	case 4:
		cout << "Quieres tener este item?" << endl;
		cout << "1. Si" << " 2. No" << endl;
		switch (yesno)
		{
		case 1:
			if (MenuItems[1]->ItemID == 1)
			{
				CWeapon* Sword = new CWeapon(1);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 2)
			{
				CWeapon* Sword = new CWeapon(2);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 3)
			{
				CWeapon* Sword = new CWeapon(1);
				Inv.AddItem(Sword);
			}
			if (MenuItems[1]->ItemID == 4)
			{
				CPotion* potion = new CPotion(1);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 5)
			{
				CPotion* potion = new CPotion(2);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 6)
			{
				CPotion* potion = new CPotion(3);
				Inv.AddItem(potion);
			}
			if (MenuItems[1]->ItemID == 7)
			{
				CArmour* armour = new CArmour(1);
				Inv.AddItem(armour);
			}
			if (MenuItems[1]->ItemID == 8)
			{
				CArmour* armour = new CArmour(2);
				Inv.AddItem(armour);
			}
			if (MenuItems[1]->ItemID == 9)
			{
				CArmour* armour = new CArmour(3);
				Inv.AddItem(armour);
			}
			cout << "Item Added" << endl;
			getchar();
		case 2:
			break;
		default:
			break;
		}
	default:
		break;
	}
}

int main()
{
	bool bNameChosen = false;
	int iSavedName = 0;
	int iID;
	string strAdventurero;
	
	CPlayer player;
	ofstream filesaveoutput;

	ifstream filesavereader("SaveData.txt");
	//istringstream iss(strAdventurero);
	string strSavedInfo;
	//filesavereader.open("SaveData.txt");
	getline(filesavereader, strSavedInfo);

	if (strSavedInfo == "")
	{}
	else
	{
			iSavedName = stoi(strSavedInfo);
	}

	//if there is a saved game
	if (iSavedName == 1)
	{
		int iline = 0;
		while(!filesavereader.eof())
		{
			getline(filesavereader, strSavedInfo); //check to see theres a saved name already
			//strAdventurero = (strSavedInfo);
			switch (iline)
			{
			case 0:
				stats.strPlayerName = (strSavedInfo);
				break;
			case 1:
				player.iPNivel = stoi(strSavedInfo);
				break;
			case 2:
				iID = stoi(strSavedInfo);
				if (iID == 1)
				{
					CWeapon* sword = new CWeapon(1);
					stats.EquipItem(sword);
				}
				else if (iID == 2)
				{
					CWeapon* sword = new CWeapon(2);
					stats.EquipItem(sword);
				}
				else if (iID == 3)
				{
					CWeapon* sword = new CWeapon(3);
					stats.EquipItem(sword);
				}
				break;
			case 3:
				iID = stoi(strSavedInfo);
				if (iID == 7)
				{
					CArmour* armour = new CArmour(1);
					stats.EquipItem(armour);
				}
				else if (iID == 8)
				{
					CArmour* armour = new CArmour(2);
					stats.EquipItem(armour);
				}
				else if (iID == 9)
				{
					CArmour* armour = new CArmour(3);
					stats.EquipItem(armour);
				}
				break;
			default:
				break;
			}
			//strAdventurero = (strSavedInfo);
		}
		filesavereader.close();
		//getline(filesavereader, strSavedInfo, '#'); //check to see theres a saved name already
		//strAdventurero = (strSavedInfo);
		
		bNameChosen = true;
	}

	if (bNameChosen == true)
	{
		cout << "welcome " + strAdventurero << endl;
	}
	else if (bNameChosen == false)
	{
		cout << "Bienvenidos Adventurero cual es tu nombre" << endl;
		cin >> strAdventurero;
		CArmour* Armour = new CArmour(1);
		EArmour.push_back(Armour);
		CWeapon* Sword = new CWeapon(1);
		EWeapon.push_back(Sword);
	}

	char cMenuItem = '1';

	vector <string> menuItem = { "1. Inventario", "2. Final Boss(pelea Nivel 20)", "3. Status", "4. Explorar", "5. Tienda", "6. Save", "7. Salir" };
	char ckeys = 'a';
	int iMenuoption = 0;
	int iMenu=0;

	// start of arrow keys menu
	while (ckeys != 'g')
	{
		system("cls");
		cout << "Elige tu opcion de Menu" << endl;
		for (int i = 0; i < 7; i++) // imprint arrow based on keys
		{
			if (iMenuoption == i)
			{
				cout << "->";

			}
			else
			{
				cout << "";
			}
			cout << menuItem[i] << endl;

		}
		ckeys = _getch();
		switch (ckeys)
		{
		case 's':
			if (iMenuoption < 7)
			{
				iMenuoption++;
				
				iMenu = iMenuoption;
			}
			break;
		case 'w':
			if (iMenuoption > 0)
			{
				iMenuoption--;
				
				iMenu = iMenuoption;
			}
			break;
		case 13:
				switch (iMenu)
				{
				case 0:
					cout << "Inventario" << endl;
					Inv.ShowInventory();
					getchar();
					break;
				case 1:
					cout << "Pelea con Final Boss" << endl;
					CombatBoss(20, 5, player);
					getchar();
					break;
				case 2:
					cout << "Status" << endl;
					stats.PrintStats();
					getchar();
					break;
				case 3:
					cout << "Explorar" << endl;
					Explore(player);
					getchar();
					break;
				case 4:
					cout << "Tienda" << endl;
					Menutienda();
					getchar();
					break;
				case 5:
					cout << "guardar" << endl;
					int idWeapon;
					int idarmour;
					idWeapon = EWeapon[0]->ItemID;
					idarmour = EArmour[0]->ItemID;
					filesaveoutput.open("SaveData.txt",ios::out ,ios::trunc);
					filesaveoutput << "1"<< endl;
					filesaveoutput << stats.strPlayerName << endl;
					filesaveoutput << player.iPNivel << endl;
					filesaveoutput << stats.EquippedWeapon->ItemID << endl;
					filesaveoutput << stats.EquippedArmour->ItemID << endl;
					filesaveoutput.close();
					getchar();
					break;
				case 6:
					cout << "salir" << endl;
					exit(0);
					getchar();
					break;
				default:
					cout << "invalid menu item" << endl;
					getchar();
					break;
				}
			break;
		default:
			break;
		}
		
	}
	//end of arrow keys menu

	//default menu in case something doesnt work
	/*while (cMenuItem != '7')
	{
		cout << "Elige tu opcion de Menu" << endl;
		for (int i = 0; i < 7; i++) // imprint arrow based on keys
		{
			if (iMenuoption == i)
			{
				cout << "->";

			}
			else
			{
				cout << "";
			}
			cout << menuItem[i] << endl;

		}

		getchar();
		cMenuItem = getchar();
		switch (cMenuItem)
		{
		case '1':
			cout << "menu 1" << endl;
			break;
		case '2':
			cout << "menu 2" << endl;
			break;
		case '3':
			cout << "menu 3" << endl;
			break;
		case '4':
			cout << "Explorar" << endl;
			Explore(iPlayerlvl);
			break;
		case '5':
			cout << "Tienda" << endl;
			Menutienda();
			break;
		case '6':
			cout << "guardar" << endl;
			filesaveoutput.open("SaveData.txt");
			filesaveoutput << "1#" + strAdventurero + "\n";
			filesaveoutput.close();
			break;
		case '7':
			cout << "salir" << endl;
			exit;
			break;
		default:
			cout << "invalid menu item" << endl;
			break;
		}
	}*/
	//end of default menu
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
