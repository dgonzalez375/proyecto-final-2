#include "Inventario.h"

using namespace std;

void CInventoryManager::AddItem(CWeapon* item)
{
	if ((WeaponInventory.size() + PotionInventory.size() + ArmourInventory.size()) != 12)
	{
		WeaponInventory.push_back(item);
	}
	else
	{
		cout << "Inventory is Full (max 12 items). So the item was not added" << endl;
	}
}

void CInventoryManager::AddItem(CPotion* item)
{
	if ((WeaponInventory.size() + PotionInventory.size() + ArmourInventory.size()) != 12)
	{
		PotionInventory.push_back(item);
	}
	else
	{
		cout << "Inventory is Full (max 12 items). So the item was not added" << endl;
	}
}

void CInventoryManager::AddItem(CArmour* item)
{
	if ((WeaponInventory.size() + PotionInventory.size() + ArmourInventory.size()) != 12)
	{
		ArmourInventory.push_back(item);
	}
	else
	{
		cout << "Inventory is Full (max 12 items). So the item was not added" << endl;
	}
}

void CInventoryManager::UseItem(int itemindex)
{
	stats.AddHp(PotionInventory[itemindex]->health);
	PotionInventory.erase(PotionInventory.begin() + itemindex);
}

void CInventoryManager::SellItem(int itemindex, int option)
{
	if (option == 0) 
	{
		stats.AddMoney(WeaponInventory[itemindex]->iValue);
		WeaponInventory.erase(WeaponInventory.begin() + itemindex);
	}
	else 
	{
		stats.AddMoney(PotionInventory[itemindex]->iValue);
		PotionInventory.erase(PotionInventory.begin() + itemindex);
	}
}

void CInventoryManager::ShowInventory()
{
	int iPlayerItemChoice = 0;
	int iPlayerInventoryChoice = 0;
	int iPlayerActionChoice = 0;

	vector<string> menuSwordAndArmour = { "1. Equip (When Equipping, the current item equipped is replaced and destroyed)", "2. Sell", "3. Return" };
	vector<string> menuPotion = { "1. Use", "2. Sell", "3. Return" };
	vector<string> menuOptions = { "1. Weapon Inventory", "2. Potion Inventory", "3. Armour Inventory", "4. Go Back"};

	for (int i = 0; i <= menuOptions.size(); i++) 
	{
		cout << menuOptions[i] << endl;
	}

	cout << "Choose what inventory to see: ";
	cin >> iPlayerInventoryChoice;

	switch (iPlayerInventoryChoice)
	{
	case 1:
		for (int i = 0; i <= WeaponInventory.size(); i++)
		{
			cout << i + 1 << ".";
			WeaponInventory[i]->print();
			cout << "  Value: " << WeaponInventory[i]->iValue;
		}

		cout << endl << "Please select a item or enter 0 to exit this menu: ";
		cin >> iPlayerItemChoice;

		for (int i = 0; i <= menuSwordAndArmour.size(); i++)
		{
			cout << menuSwordAndArmour[i] << endl;
		}

		cout << "Please select what do you wish to do with the selected item: ";
		cin >> iPlayerActionChoice;

		switch (iPlayerActionChoice)
		{
		case 1:
			stats.EquipItem(WeaponInventory[iPlayerItemChoice]);
			break;
		case 2:
			SellItem(iPlayerItemChoice, 0);
			break;
		default:
			ShowInventory();
			break;
		}

		break;
	case 2:
		for (int i = 0; i <= PotionInventory.size(); i++)
		{
			cout << i + WeaponInventory.size() << ".";
			WeaponInventory[i]->print();
		}

		cout << endl << "Please select a item or enter 0 to exit this menu: ";
		cin >> iPlayerItemChoice;

		for (int i = 0; i <= menuPotion.size(); i++)
		{
			cout << menuPotion[i] << endl;
		}

		cout << "Please select what do you wish to do with the selected item: ";
		cin >> iPlayerActionChoice;

		switch (iPlayerActionChoice)
		{
		case 1:
			UseItem(iPlayerItemChoice);
			break;
		case 2:
			SellItem(iPlayerItemChoice, 1);
			break;
		default:
			ShowInventory();
			break;
		}
		break;
	case 3:
		for (int i = 0; i <= ArmourInventory.size(); i++)
		{
			cout << i + 1 << ".";
			ArmourInventory[i]->print();
			cout << "  Value: " << ArmourInventory[i]->iValue;
		}

		cout << endl << "Please select a item or enter 0 to exit this menu: ";
		cin >> iPlayerItemChoice;

		for (int i = 0; i <= menuSwordAndArmour.size(); i++)
		{
			cout << menuSwordAndArmour[i] << endl;
		}

		cout << "Please select what do you wish to do with the selected item: ";
		cin >> iPlayerActionChoice;

		switch (iPlayerActionChoice)
		{
		case 1:
			stats.EquipItem(ArmourInventory[iPlayerItemChoice]);
			break;
		case 2:
			SellItem(iPlayerItemChoice, 0);
			break;
		default:
			ShowInventory();
			break;
		}
		break;
	default:
		break;
	}
}
